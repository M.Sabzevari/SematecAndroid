package sematec.dicapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import sematec.dicapp.models.OxfordModel;

public class MainActivity extends AppCompatActivity {

    EditText word;
    TextView meaningOfWord;
    TextView results ;
    DictionaryDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        word = (EditText) findViewById(R.id.word_id);
        meaningOfWord = (TextView) findViewById(R.id.meaning_id);
        results = (TextView) findViewById(R.id.result_id) ;
        dbHandler = new DictionaryDBHandler( this ,"dic.db" ,null , 1) ;


        findViewById(R.id.show_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url;
                String wordParam = word.getText().toString();
                String language = "en";
                String word_id = wordParam.toLowerCase();
                url = "https://od-api.oxforddictionaries.com:443/api/v1/entries/" + language + "/" + word_id;
                String outStr ;

                outStr = dbHandler.getValues(wordParam) ;

                if (outStr != "")
                {results.setText("Meaning of this word is fetched from database");

                }
                else{
                    getMeaningByAsync(url);
                    results.setText("Meaning of this word is fetched from Longman Dictionary api");
                    dbHandler.insertWord(word_id , results.getText().toString() );} ;





            }
        });

    }

    void getMeaningByGson(String serverResponse) {

        Gson gson = new Gson();
        OxfordModel meaningModel = gson.fromJson(serverResponse, OxfordModel.class);

        meaningOfWord.setText(meaningModel.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString());

    }


    public void getMeaningByAsync(String url) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Accept", "application/json");
        client.addHeader("app_id", "f29a6f23");
        client.addHeader("app_key", "670f72b7f47b56c45504a745bde9be2d");
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.toString(), Toast.LENGTH_LONG).show();
                meaningOfWord.setText("I Can't find this word");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                getMeaningByGson(responseString);

            }
        });

    }


}

package sematec.dicapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Asus N on 11/2/2017.
 */

public class DictionaryDBHandler extends SQLiteOpenHelper {

    String tblQuery = " CREATE TABLE wordtbl( " +
            " _id INTEGER AUTO INCREMENT PRIMARY KEY , " +
            " wordcol TEXT , " +
            " meaningcol TEXT " +
            ")";


    public DictionaryDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tblQuery);
    }

    public void insertWord(String word, String meaning) {
        String insertQuery = "INSERT INTO wordtbl ( wordcol , meaningcol ) " +
                " VALUES( '" + word + "' , '" + meaning + "'  )";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();
    }

    public String getValues(String word) {

        String results = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT meaningcol FROM  wordtbl  WHERE wordcol='" + word + "'";
        Cursor cursor = db.rawQuery(selectQuery ,null) ;
        while (cursor.moveToNext()) {
            results = cursor.getString(0) ;
        }


        return results ;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}

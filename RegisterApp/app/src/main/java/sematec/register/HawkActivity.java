package sematec.register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

public class HawkActivity extends BaseActivity {

    TextView hawk;
    ImageView img ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawk);

        Hawk.init(mContext).build();
        Hawk.put("my_name" , "Mozaffar Sabzevari"); ;
        hawk = (TextView) findViewById(R.id.hawk);
        img = (ImageView ) findViewById(R.id.img) ;


        Picasso.with( mContext)
                .load("https://www.irib.ir/assets/slider_images/20170906090915_2046.png")
                .into(img) ;


        String res = Hawk.get("my_name") ;

        hawk.setText(res);

    }
}

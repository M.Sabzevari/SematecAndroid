package sematec.register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonArrayActivity extends AppCompatActivity {

    TextView listName;
    Button showList;
    String jsonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_array);

        listName = (TextView) findViewById(R.id.json_id);
        showList = (Button) findViewById(R.id.sh1);
        jsonResponse = " {\"abridged_cast\": [ " +
                "            { " +
                "                \"name\": \"Jeff Bridges\", " +
                "                \"id\": \"162655890\", " +
                "                \"characters\": [ " +
                "                    \"Jack Prescott\" " +
                "                ] " +
                "            }, " +
                "            { " +
                "                \"name\": \"Charles Grodin\", " +
                "                \"id\": \"162662571\", " +
                "                \"characters\": [ " +
                "                    \"Fred Wilson\" " +
                "                ] " +
                "            }, " +
                "            { " +
                "                \"name\": \"Jessica Lange\", " +
                "                \"id\": \"162653068\", " +
                "                \"characters\": [ " +
                "                    \"Dwan\" " +
                "                ] " +
                "            }, " +
                "            { " +
                "                \"name\": \"John Randolph\", " +
                "                \"id\": \"162691889\", " +
                "                \"characters\": [ " +
                "                    \"Capt. Ross\" " +
                "                ] " +
                "            }, " +
                "            { " +
                "                \"name\": \"Rene Auberjonois\", " +
                "                \"id\": \"162718328\", " +
                "                \"characters\": [ " +
                "                    \"Bagley\"  " +
                "                ] " +
                "            } " +
                "        ]  }";


        showList.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            try {
                                               JSONObject jsonRoot = new  JSONObject(jsonResponse);
                                                String allNames ="";
                                                JSONArray cast = jsonRoot.getJSONArray("abridged_cast") ;
                                                //JSONArray cast = new JSONArray(jsonResponse);

                                                for (int i=0 ; i<cast.length() ; ++i ){
                                                    JSONObject actor = cast.getJSONObject(i) ;
                                                    String name = actor.getString("name") ;
                                                    allNames = allNames + "\n" + name ;
                                                }
                                                listName.setText(allNames) ;

                                            } catch (Throwable tx) {
                                                Log.e("My App", "Could not parse malformed JSON ");
                                            };

                                        }
                                    }
        );


    }

    ;
}

package sematec.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    Button confirmId;
    EditText nameId;
    EditText familyId;
    EditText mobileId;
    TextView summaryId;
    String varInform;
    Button startSecondActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binViews();
    }


    private void binViews() {
        confirmId = (Button) findViewById(R.id.confirmId);
        nameId = (EditText) findViewById(R.id.nameId);
        familyId = (EditText) findViewById(R.id.familyId);
        mobileId = (EditText) findViewById((R.id.mobileId));
        summaryId = (TextView) findViewById(R.id.summaryId);
        startSecondActivity = (Button) findViewById(R.id.startSecondActivity);

        confirmId.setOnClickListener(this);
        startSecondActivity.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startSecondActivity) {
            Intent secondActivityIntent = new Intent(mContext, SecondActivity.class);
            secondActivityIntent.putExtra("name", nameId.getText().toString());
            secondActivityIntent.putExtra("family", familyId.getText().toString());
            secondActivityIntent.putExtra("mobile", mobileId.getText().toString());
            startActivity(secondActivityIntent);

        } else if (view.getId() == R.id.confirmId) {

            varInform = nameId.getText().toString().trim() + "\n" +
                    familyId.getText().toString().trim() + "\n" +
                    mobileId.getText().toString();
             summaryId.setText(varInform);
        }


    }

}

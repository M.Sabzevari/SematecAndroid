package sematec.register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import sematec.register.models.YahooModel;

public class WeatherYahooActivity extends AppCompatActivity {
    EditText cityName;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_yahoo);

        cityName = (EditText) findViewById(R.id.cityName);
        result = (TextView) findViewById(R.id.result);

        findViewById(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                String city = cityName.getText().toString();
                final String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                        city + "%2Cir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

                getWheaterByAsync(url);

//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        getWeather(url);
//                    }
//                }).start();


            }
        });
    }

    public void getWheaterByAsync(String url) {

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(WeatherYahooActivity.this, throwable.toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                getShowTempByGson(responseString);
            }
        });
    }


    public void getWeather(String url) {


        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //getShowTemp(response.toString());
                        getShowTempByGson(response.toString());
                    }
                });

            }
        } catch (Exception e) {
            Log.d("", "getWeather: Exception:" + e);
        }

    }


    void getShowTempByGson(String serverResponse) {

        Gson gson = new Gson();
        YahooModel weather = gson.fromJson(serverResponse, YahooModel.class);

        if (weather.getQuery().getCount() >= 1)
            result.setText(cityName.getText().toString() + " : " + weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
        else
            Toast.makeText(this , "city wrong" , Toast.LENGTH_SHORT).show();

    }


    void getShowTemp(String serverResponse) {

        try {
            JSONObject allObj = new JSONObject(serverResponse);

            String queryStr = allObj.getString("query");
            JSONObject queryObj = new JSONObject(queryStr);

            String resultsStr = queryObj.getString("results");
            JSONObject resultsObj = new JSONObject(resultsStr);

            String channelStr = resultsObj.getString("channel");
            JSONObject channelObj = new JSONObject(channelStr);

            String itemStr = channelObj.getString("item");
            JSONObject itemObj = new JSONObject(itemStr);

            String conditionStr = itemObj.getString("condition");
            JSONObject conditionObj = new JSONObject(conditionStr);

            String tempStr = conditionObj.getString("temp");


            result.setText(cityName.getText().toString() + " : " + String.valueOf((Integer.parseInt(tempStr) - 32) * 0.5556));
        } catch (Exception e) {
            Log.d("", "getWeather: Exception:" + e);
        }


    }
}

package sematec.register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import sematec.register.adapters.StudentListAdapter;

public class MyListViewActivity extends BaseActivity {

    ListView myList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list_view);

        myList = (ListView) findViewById(R.id.myList);
        String names[] = {"Ali", "Reza", "Babak", "Amin"};

        String avatars[] = {
                "https://www.irib.ir/assets/slider_images/20170906090915_2046.png",
                "https://www.irib.ir/assets/slider_images/20170906090915_2046.png",
                "https://www.irib.ir/assets/slider_images/20170906090915_2046.png",
                "https://www.irib.ir/assets/slider_images/20170906090915_2046.png"};


        StudentListAdapter adapter = new StudentListAdapter(mContext, names ,avatars);

        myList.setAdapter(adapter);

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String clikedItem = (String) adapterView.getItemAtPosition(position);

                showToast(clikedItem);


            }
        });


    }
}

package sematec.register;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Asus N on 10/19/2017.
 */

public class Publics {


    public static void setShared(Context mContext , String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply();
        ;

    }

    public static   String getShared(Context mContext , String key, String def_value) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, def_value);
    }
}

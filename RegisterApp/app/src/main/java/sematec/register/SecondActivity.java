package sematec.register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

/**
 * Created by Asus N on 10/19/2017.
 */

public class SecondActivity extends BaseActivity  {

    TextView result ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        bindViews() ;

        String res = getIntent().getStringExtra("name") +" " +
                     getIntent().getStringExtra("family") + " " +
                     getIntent().getStringExtra("mobile") ;
        result.setText(res);
    }


    void bindViews(){
     result = (TextView) findViewById(R.id.result);


    }

}

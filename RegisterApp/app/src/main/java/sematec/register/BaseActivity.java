package sematec.register;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by Asus N on 10/19/2017.
 */

public class BaseActivity extends AppCompatActivity {

    Context mContext = this;
    Activity mActivity = this;

    public void  showToast (String txt) {
        Toast.makeText(mContext,txt ,Toast.LENGTH_SHORT).show();
    }

}

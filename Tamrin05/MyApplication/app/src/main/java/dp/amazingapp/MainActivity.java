package dp.amazingapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText urlText;
    Button showSite;
    WebView myWeb;
    String urlParam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        urlText = (EditText) findViewById(R.id.urlText);
        findViewById(R.id.showSite).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                myWeb = (WebView) findViewById(R.id.myWeb);
                myWeb.getSettings().setJavaScriptEnabled(true);
                myWeb.setWebViewClient(new WebViewClient());

                urlParam = urlText.getText().toString().toLowerCase();

                if (!isValidUrl(urlParam))
                    urlParam = "http://" + urlParam;

                myWeb.loadUrl(urlParam);
            }
        });


    }


    public boolean isValidUrl(String urlString) {
        return urlString.contains("http");
          
    }
}
